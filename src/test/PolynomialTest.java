import static org.junit.Assert.*;
import org.junit.Test;

public class PolynomialTest {
    @Test
    public void getDegreeEmpty() {
        Polynomial a = new Polynomial();
        assertEquals(0, a.getDegree());
    }

    @Test
    public void getDegreeNumber() {
        Polynomial a = new Polynomial(42);
        assertEquals(1, a.getDegree());
    }

    @Test
    public void getDegreeCubic() {
        Polynomial a = new Polynomial(7, 8, 9, 10);
        assertEquals(3, a.getDegree());
    }

    @Test
    public void getCoefficientQuadratic() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals(7, a.getCoefficient(0));
        assertEquals(8, a.getCoefficient(1));
        assertEquals(9, a.getCoefficient(2));
    }

    @Test
    public void getCoefficientOutOfRange() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals(0, a.getCoefficient(3));
        assertEquals(0, a.getCoefficient(4));
    }

    @Test
    public void toStringNumber() {
        Polynomial a = new Polynomial(42);
        assertEquals("Polynomial[42]", a.toString());
    }

    @Test
    public void toStringQuadratic() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals("Polynomial[7,8,9]", a.toString());
    }

    @Test
    public void toPrettyStringQuadratic() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals("9x^2 + 8x + 7", a.toPrettyString("x"));
    }

    @Test
    public void toPrettyStringQuadraticWithY() {
        Polynomial a = new Polynomial(7, 8, 9);
        assertEquals("9y^2 + 8y + 7", a.toPrettyString("y"));
    }

    @Test
    public void toPrettyStringWithOne() {
        Polynomial a = new Polynomial(1, 2, 1);
        assertEquals("x^2 + 2x + 1", a.toPrettyString("x"));
    }

    @Test
    public void toPrettyStringWithZero() {
        Polynomial a = new Polynomial(3, 0, 5);
        assertEquals("5x^2 + 3", a.toPrettyString("x"));
    }

    @Test
    public void toPrettyStringWithNegative() {
        Polynomial a = new Polynomial(17, 0, -2, -5);
        assertEquals("-5x^3 - 2x^2 + 17", a.toPrettyString("x"));
    }
    @Test
    public void suma() {
    	Polynomial a = new Polynomial(1, 2, 3, 4);
    	Polynomial b = new Polynomial(2, 3, 4, 5);
    	Polynomial c = Polynomial.sum(a, b);
    	assertEquals("9x^3 + 7x^2 + 5x + 3", c.toPrettyString("x"));
    }
    @Test
    public void sumaWithZero() {
    	Polynomial a = new Polynomial(1, 2, 3, 4);
    	Polynomial b = new Polynomial(0);
    	Polynomial c = Polynomial.sum(a, b);
    	assertEquals("4x^3 + 3x^2 + 2x + 1", c.toPrettyString("x"));
    }
    public void sumaWithNegative() {
    	Polynomial a = new Polynomial(1, 2, 3, 4);
    	Polynomial b = new Polynomial(-2, -3, -4, -5);
    	Polynomial c = Polynomial.sum(a, b);
    	assertEquals("- 1x^3 - 1x^2 - 1x - 1", c.toPrettyString("x"));
    }
    @Test
    public void multiply() {
    	Polynomial a = new Polynomial(1, 2, 3);
    	Polynomial b = new Polynomial(2, 1);
    	Polynomial c = Polynomial.product(a, b);
    	assertEquals("3x^3 + 8x^2 + 5x + 2", c.toPrettyString("x"));
    }
    @Test
    public void multiplyWithZero() {
    	Polynomial a = new Polynomial(1, 2, 3);
    	Polynomial b = new Polynomial(0);
    	Polynomial c = Polynomial.product(a, b);
    	assertEquals("", c.toPrettyString("x"));
    }
    @Test
    public void multiplyWithNegative() {
    	Polynomial a = new Polynomial(1, 2, 3);
    	Polynomial b = new Polynomial(-3, -1);
    	Polynomial c = Polynomial.product(a, b);
    	assertEquals("-3x^3 - 11x^2 - 7x - 3", c.toPrettyString("x"));
    }
    @Test
    public void divide() {
    	Polynomial a = new Polynomial(-12, -4, 3, 1);
    	Polynomial b = new Polynomial(-6, 1, 1);
    	Polynomial c = Polynomial.div(a, b);
    	assertEquals("x + 2", c.toPrettyString("x"));
    }
    @Test
    public void divideWithRemainder() {
    	Polynomial a = new Polynomial(9, 2, 7, 2);
    	Polynomial b = new Polynomial(3, 2);
    	Polynomial rem = new Polynomial();
    	Polynomial c = Polynomial.div(a, b);
    	rem = Polynomial.mod(a, b);
    	assertEquals("x^2 + 2x - 2", c.toPrettyString("x"));
    	assertEquals("15", rem.toPrettyString("x"));
    }
    @Test
    public void divideWithBiggerDivisor() {
    	Polynomial a = new Polynomial(9, 2, 7, 2);
    	Polynomial b = new Polynomial(3, 9, 10, 2, 4);
    	Polynomial rem = new Polynomial();
    	Polynomial c = Polynomial.div(a, b);
    	rem = Polynomial.mod(a, b);
    	assertEquals("", c.toPrettyString("x"));
    	assertEquals("2x^3 + 7x^2 + 2x + 9", rem.toPrettyString("x"));
    }
    //(dividing with 0 in first, second or result solved in main, idk how to test that)
    
}
