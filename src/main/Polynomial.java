
/** Integer-only polynomials. */
public class Polynomial {
	private int coefficients [];

    /** Create new instance with given coefficients.
     *
     * x^2 + 3x - 7 would be created by new Polynomial(-7, 3, 1)
     * x^5 + 42 would be created by new Polynomial(42, 0, 0, 0, 0, 1)
     *
     * @param coef Coefficients, ordered from lowest degree (power).
     */
    public Polynomial(int... coef) {                    //TADY V TOM NAFLUSAT CISLA DO TOHO POLE
        System.out.printf("Creating polynomial");
        coefficients = new int[coef.length];
        for (int i = 0; i < coef.length; i++) {
        	coefficients[i]=coef[i];
            if (i > 0) {
                System.out.print(" +");
            }
            System.out.printf(" %d * x^%d", coef[i], i);
        }
        System.out.println(" ...");
    }

    /** Get coefficient value for given degree (power). */
    public int getCoefficient(int degree) {
        if(degree <= coefficients.length - 1) {
        	return coefficients[degree];
        }else{
        	return 0;
        }
    }

        
    

    /** Get degree (max used power) of the polynomial. */
    public int getDegree() {
		if (coefficients.length == 1) {
			return 1;
		}
		if (coefficients.length <= 0) {
			return 0;
		}
		for (int i = coefficients.length - 1; i >= 0; i--) {
			if (coefficients[i] != 0) {
				return i;
			}
		}
		return coefficients.length - 1;
	}



    /** Format into human-readable form with given variable. */
    public String toPrettyString(String variable) {
        String prettyString = "";
        if(coefficients.length > 0) {
        	for(int i = coefficients.length - 1; i >= 0; i--) {
        		String variableExp = "";
        		String nomial = "";
        		if (i > 1) {
        			variableExp = String.format(variable + "^%d", i);
        		} else if (i == 1) {
        			variableExp = variable;
        		} else if (i == 0) {
        			variableExp = "";
        		}
        		
        		if(coefficients[i] != 0 && Math.abs(coefficients[i]) != 1) {
        			nomial = String.format("%d%s", Math.abs(coefficients[i]), variableExp);
        		} else if (coefficients[i] == 0) {
        			continue;
        		} else if (Math.abs(coefficients[i]) == 1 && i != 0) {
        			nomial = String.format("%s", variableExp);
        		} else if (i == 0) {
        			nomial = String.format("%d", Math.abs(coefficients[i]));
        		}
        		
        		if(prettyString.length() > 0) {
        			if (coefficients[i] > 0) {
        				prettyString = prettyString + " + " + nomial;
        			} else {
        				prettyString = prettyString + " - " + nomial;}
        		} else {
        			if (coefficients[i] > 0) {
        				prettyString = nomial;
        			}else{
        				prettyString = "-" + nomial;
        			}
        		}
        	}
        }
        return prettyString;
    }


    /** Debugging output, dump only coefficients. */
    @Override
    public String toString() {
        String coefs = "";
        for(int i = 0; i <= coefficients.length - 1; i++) {
        	if(coefs.length() == 0) {
        		coefs = String.format("%d", coefficients[i]);
        	}else {
        		coefs = coefs + "," + String.format("%d", coefficients[i]);
        	}
        }
    	String returnString = String.format("Polynomial[%s]", coefs);
    	return returnString;
    }


    /** Adds together given polynomials, returning new one. */
    public static Polynomial sum(Polynomial... polynomials) {
		int[] sum = new int[1];
		if (polynomials[0].getDegree() > polynomials[1].getDegree()) {
			sum = new int[polynomials[0].getDegree() + 1];
		}else {
			sum = new int[polynomials[1].getDegree() + 1];
		}
		
		for (int i = 0; i < sum.length; i++) {
			sum[i] = polynomials[0].getCoefficient(i) + polynomials[1].getCoefficient(i);
		}
		return new Polynomial(sum);
	}


    /** Multiplies together given polynomials, returning new one. */
    public static Polynomial product(Polynomial... polynomials) {
    	int[] product = new int[(polynomials[0].getDegree()+1 * (polynomials[1].getDegree()+1))];
    	if((polynomials[0].getDegree() == 0 && polynomials[0].getCoefficient(0) == 0) || (polynomials[1].getDegree() == 0 && polynomials[1].getCoefficient(0) == 0)){
    		product[0] = 0;
    	}
    	

        int positionInResult = 0;
        for(int i = 0; i < polynomials[0].getDegree()+1; i++) {
        	for(int j = 0; j < polynomials[1].getDegree()+1; j++) {
        		positionInResult = i+j;
        		int coeff = polynomials[0].getCoefficient(i) * polynomials[1].getCoefficient(j);
        		product[positionInResult] += coeff;
        	}
        }
    	return new Polynomial(product);
    }

    /** Get the result of division of two polynomials, ignoring remainder. */
    public static Polynomial div(Polynomial dividend, Polynomial divisor) {
       int[] quotient = new int[dividend.getDegree() - divisor.getDegree() + 1];
       
	   Polynomial temporaryQuotient = dividend;
		       for(int i = 0; i < dividend.getDegree()-divisor.getDegree()+1; i++) {
		    	   int coef = temporaryQuotient.getCoefficient(temporaryQuotient.getDegree()) / divisor.getCoefficient(divisor.getDegree());
		    	   int diff = 1;
		    	   quotient[dividend.getDegree() - divisor.getDegree() - i] = coef;
		    	   int[] toAlign = new int[temporaryQuotient.getDegree()];
		    	   toAlign[temporaryQuotient.getDegree()-diff-(-1 + divisor.getDegree())] = 1;
		    	   Polynomial aligner = new Polynomial(toAlign);
		    	   Polynomial coeff = new Polynomial(coef);
		    	   Polynomial toSubstract = product(coeff, divisor);
		    	   Polynomial realToSubstract = product(toSubstract, aligner);
		    	   temporaryQuotient = sum(temporaryQuotient, realToSubstract.negative());
		    	   diff = temporaryQuotient.getDegree() - realToSubstract.getDegree();
		       } 	   
	       
       return new Polynomial(quotient);
    }

    /** Get the remainder of division of two polynomials. */
    public static Polynomial mod(Polynomial dividend, Polynomial divisor) {
		return sum(dividend, product(divisor, div(dividend, divisor)).negative());
	}

    public Polynomial negative() {
		int[] negativeCoef = new int[coefficients.length];
		for (int i = 0; i < coefficients.length; i++) {
			negativeCoef[i] = - coefficients[i];
		}
		return new Polynomial(negativeCoef);
	}

}

