import static org.junit.Assert.assertEquals;

public class Main {
    public static void main(String[] args) {
        if (args.length < 4) {
            printHelp();
            return;
        }

        String op	 = args[0];

        int delimPosition = 0;
        for (int i = 1; i < args.length; i++) {
            if (args[i].equals("--")) {
                delimPosition = i;
                break;
            }
        }
        if (delimPosition == 0) {
            printHelp();
            return;
        }

        int[] coefFirst = parseCoefficients(args, 1, delimPosition);
        int[] coefSecond = parseCoefficients(args, delimPosition + 1, args.length);

        if ((coefFirst.length == 0) || (coefSecond.length == 0)) {
            printHelp();
            return;
        }

        Polynomial first = new Polynomial(coefFirst);
        Polynomial second = new Polynomial(coefSecond);

        Polynomial result;
        Polynomial remainder = null;
        String opSymbol;
        boolean zero = false;
        boolean noResult = false;
        if (op.equals("plus")) {
            result = Polynomial.sum(first, second);
            opSymbol = "+";
        } else if (op.equals("times")) {
            result = Polynomial.product(first, second);
            opSymbol = "*";
        } else if (op.contentEquals("divide")){
        	if(first.getDegree() == 1 && first.getCoefficient(0) == 0) {
        		zero = true;
        		opSymbol = "";
        		result = null;
        	}else if(second.getDegree() == 1 && second.getCoefficient(0) == 0) {
        		noResult = true;
        		opSymbol = "";
        		result = null;
        	}else {
        	
	        	result = Polynomial.div(first, second);
	        	remainder = Polynomial.mod(first, second);
	        	opSymbol = ":";
        	}
        } else {
            printHelp();
            return;
        }
        if(zero) {
        	System.out.printf("(%s) %s (%s) = (%s)", 
            		first.toPrettyString("x"), 
            		opSymbol, 
            		second.toPrettyString("x"),
    				"0");
        }else if(noResult) {
        	System.out.print("Can't divide with zero");
        }else if(result.getCoefficient(0) == 0){
        	System.out.printf("(%s) %s (%s) = (%s)", 
	        		first.toPrettyString("x"), 
	        		opSymbol, 
	        		second.toPrettyString("x"),
					"0");
	        if(remainder != null) {
	        	if(remainder.getDegree() == 1 && remainder.getCoefficient(0) == 0){
	        	System.out.print(", no remainder");
	        	}else{
	        		System.out.printf(", remainder = %s", remainder.toPrettyString("x"));
	        	}
	        }
        }else{
	        System.out.printf("(%s) %s (%s) = (%s)", 
	        		first.toPrettyString("x"), 
	        		opSymbol, 
	        		second.toPrettyString("x"),
					result.toPrettyString("x"));
	        if(remainder != null) {
	        	if(remainder.getDegree() == 1 && remainder.getCoefficient(0) == 0){
	        	System.out.print(", no remainder");
	        	}else{
	        		System.out.printf(", remainder = %s", remainder.toPrettyString("x"));
	        	}
	        }
        }
    }

    private static void printHelp() {
        System.out.println("Usage: ");
        System.out.println("The first argument stands for the operation to be done (\"plus\" for addition, \"times\" for multiplication and \"divide\" for division).");
        System.out.println("");
        System.out.println("The following arguments are the coefficients of the first polynomial, beginning with the highest power.");
        System.out.println("All of those have to be integers and have to be split by spaces.");
        System.out.println("The individual polynomials are divided by \"--\".");
        System.out.println("Then follow the coefficients of the other polynomial/s in the same order as the first.");
        System.out.println("");
        System.out.println("Example:");
        System.out.println("To calculate \"(6x^4 + 4x^3 - 3x^2 + 1) + (x^3 - 2x^2 + 7x - 8)\", type in \"plus 6 4 -3 0 1 -- 1 -2 7 -8\".");
        
        // TODO
    }

    private static int[] parseCoefficients(String[] args, int firstIndexIncl, int lastIndexExcl) {
        int[] result = new int[lastIndexExcl - firstIndexIncl];
        int resIndex = result.length - 1;
        for (int i = firstIndexIncl; i < lastIndexExcl; i++) {
            result[resIndex] = Integer.parseInt(args[i]);
            resIndex--;
        }
        return result;
    }
    
}
